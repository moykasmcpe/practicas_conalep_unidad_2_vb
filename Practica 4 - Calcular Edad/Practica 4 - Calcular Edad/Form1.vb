﻿Public Class Form1

    'Práctica 4 - UNIDAD 2 - Calcular Edad
    'Creador: Moykas
    'Canal YT: https://youtube.com/channel/UC2ENP9NYr0u8-MrgxD1gF3g
    'Sitio Web: https://mooykas.000webhostapp.com/

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        'Asignamos variables
        Dim datonac As Date = CDate(Me.DateTimePicker1.Value)
        Dim datoact As Date = CDate(Me.DateTimePicker2.Value)

        'Validamos que las fechas sean diferentes
        'datonac debe ser menor a datoact
        If datonac < datoact Then
            TextBox1.Text = (datoact.Year - datonac.Year)
        Else
            'Mensaje de error
            MsgBox("Error - Fecha no valida", MsgBoxStyle.MsgBoxHelp)
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Limpiamos valor
        TextBox1.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'Cerramos o detenemos programa
        End
    End Sub
End Class
