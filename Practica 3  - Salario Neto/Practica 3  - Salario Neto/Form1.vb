﻿Public Class Form1

    'Practica 3 - UNIDAD 2 - Salario Neto
    'Creador: Moykas
    'Canal YT: https://youtube.com/channel/UC2ENP9NYr0u8-MrgxD1gF3g
    'Sitio Web: https://mooykas.000webhostapp.com/

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Operaón

        TextBox4.Text = Val(TextBox2.Text) * Val(TextBox3.Text) 'TextBox4.Text tendra el resultado de la multipliación de TextBox2.Text y TextBox3.Text
        TextBox5.Text = 0.2 * Val(TextBox4.Text) 'Multipliación del valor TextBox4.Text por 0.2
        TextBox6.Text = Val(TextBox4.Text) - Val(TextBox5.Text) 'TextBox6.Text recibe el resultado de la resta entre TextBox4.Text y TextBox5.Text

        'Convertimos los valores Enteros a Double (flotante)
        Dim box3 As Double = TextBox3.Text
        Dim box4 As Double = TextBox4.Text
        Dim box5 As Double = TextBox5.Text
        Dim box6 As Double = TextBox6.Text

        'Formateamos los valores a decimales con precición de 2
        TextBox3.Text = FormatNumber(box3, 2)
        TextBox4.Text = FormatNumber(box4, 2)
        TextBox5.Text = FormatNumber(box5, 2)
        TextBox6.Text = FormatNumber(box6, 2)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Reemplazamos valores a vacio
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'Cerramos o cancelamos procesos
        End
    End Sub
End Class
