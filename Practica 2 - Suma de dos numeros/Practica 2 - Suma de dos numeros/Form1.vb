﻿Public Class Form1

    'Práctica 2 - UNIDAD 2 - Suma de dos numeros
    'Creador: Moykas
    'Canal YT: https://youtube.com/channel/UC2ENP9NYr0u8-MrgxD1gF3g
    'Sitio Web: https://mooykas.000webhostapp.com/

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox3.Text = Val(TextBox1.Text) + Val(TextBox2.Text)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
    End Sub
End Class
